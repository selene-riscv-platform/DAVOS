# DAVOS - a toolkit for Dependability assessment, verification, optimization and selection of hardware models. #

## Overview 

Legacy support for Zynq 7-series autonomous FPGA fault injector.



## References
Dataset illustrating dependability benchmarking of soft-core processors (MC8051, AVR, Microblaze):  https://doi.org/10.5281/zenodo.3996297


