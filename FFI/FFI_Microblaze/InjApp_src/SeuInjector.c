 /*
   Copyright (c) 2018 by Universitat Politecnica de Valencia.
   This file is a part of the DAVOS toolkit
   and is released under the "MIT license agreement".
   Please check the LICENSE.txt file (that is included as a part of this package) for the license details.
   ------------------------------------------------------------------------------------------------------
   Description:
      An library for implementing FPGA-based fault injection tools
      that access Configuration memory through the ICAP interface

   Author: Ilya Tuzov, Universitat Politecnica de Valencia
   ------------------------------------------------------------------------------------------------------
*/

#include "platform.h"
#include <xil_types.h>
#include <xil_assert.h>
#include <xhwicap.h>
#include "xparameters.h"
#include <stdio.h>
#include <stdlib.h>
#include <xil_printf.h>
#include "xgpio.h"
#include "SeuInjector.h"
#include "time.h"
#include "sleep.h"
#include "mapping.cdf"

FarFields parseFAR(u32 FAR, FpgaSeries series){
	FarFields res;
	if(series==S7){
		res.BLOCK 	= (FAR >> 23) & 0x00000007;
		res.TOP 	= (FAR >> 22) & 0x00000001;
		res.HCLKROW	= (FAR >> 17) & 0x0000001F;
		res.MAJOR 	= (FAR >> 7)  & 0x000003FF;
		res.MINOR 	=  FAR  	  & 0x0000007F;
	}
	else if(series==US){
		res.BLOCK 	= (FAR >> 23) & 0x00000007;
		res.TOP 	= 0;
		res.HCLKROW	= (FAR >> 17) & 0x0000003F;
		res.MAJOR 	= (FAR >> 7)  & 0x000003FF;
		res.MINOR 	=  FAR  	  & 0x0000007F;
	}
	else if(series==USP){
		res.BLOCK 	= (FAR >> 24) & 0x00000007;
		res.TOP 	= 0;
		res.HCLKROW	= (FAR >> 18) & 0x0000003F;
		res.MAJOR 	= (FAR >> 8)  & 0x000003FF;
		res.MINOR 	=  FAR  	  & 0x000000FF;
	}
	return(res);
}



//label: 0-7 (A-H), 8-15 (A2-H2)
FF_CBL get_ff_clb(u32 Y, u32 label, u32 FAR){
	FF_CBL res;
	u32 c = Y % 60;
	u32 group = c / 2;
	u32 top = c % 2;
	u32 idx = top*16 + label;
	u32 base_word = group<15 ? group*3 : (group*3) + 3;
	res.rb_far = (FAR & 0xFFFFFF00) | 0x0C;
	res.rb_word = base_word + rb_os_word[idx];
	res.rb_bit = rb_os_bit[idx];
	res.sr_far = (FAR & 0xFFFFFF00) | 0x0C;
	res.sr_word = base_word + sr_os_word[idx];
	res.sr_bit = sr_os_bit[idx];
	res.inv_far = (FAR & 0xFFFFFF00) | inv_os_far[idx];
	res.inv_word = base_word + inv_os_word[idx];
	res.inv_bit = inv_os_bit[idx];
	res.iclk_far = (FAR & 0xFFFFFF00) | iclk_os_far[idx];
	res.iclk_word = base_word + iclk_os_word[idx];
	res.iclk_bit = iclk_os_bit[idx];
	res.dcon_far = (FAR & 0xFFFFFF00) | dcon_os_far[idx];
	res.dcon_word = base_word + dcon_os_word[idx];
	res.dcon_bit = dcon_os_bit[idx];
	res.enainv_far = (FAR & 0xFFFFFF00) | 0x0F;
	res.enainv_word = base_word + enainv_os_word[idx];
	res.enainv_bit = enainv_os_bit[idx];
	return res;
};



void disconnect_SliceReg(InjectorDescriptor * InjDesc, u32 Y, u32 label, u32 FAR){
	FF_CBL ff = get_ff_clb(Y, label, FAR);
	FPGA_ReadFrame(&(InjDesc->HwIcap), SLR_IDX, ff.dcon_far, InjDesc->FrameData, 0);
	InjDesc->FrameData[ff.dcon_word] ^= (1 << ff.dcon_bit);
	FPGA_WriteFrame(&(InjDesc->HwIcap), SLR_IDX, InjDesc->SlrId[SLR_IDX], ff.dcon_far, InjDesc->FrameData, 0);
	printf("disconnect_SliceReg: LUT[%d] (%08x, %d, %d) set to %d\n",label, ff.dcon_far, ff.dcon_word, ff.dcon_bit,  (InjDesc->FrameData[ff.dcon_word] >> ff.dcon_bit) & 0x1);
}




//Returns current state of 16 CLB Flip-Flops [15:0] = [ H2,G2,F2,E2,D2,C2,B2,A2, H,G,F,E,D,C,B,A ]
u16 readback_slice_reg(InjectorDescriptor * InjDesc, u32 Y, u32 FAR){
	//u32 frame_buffer[FRAME_SIZE];
	FPGA_ReadFrame(&(InjDesc->HwIcap), SLR_IDX, FAR, InjDesc->Buffer, 1);
	//for(int i=0;i<93;i++) printf("readback_slice_reg.word[%d]=%08x\n\r", i, frame_buffer[i]);
	u16 result = 0x0;
	for(int i=0;i<16;i++){
		FF_CBL ff = get_ff_clb(Y, i, FAR);
		u16 v = (InjDesc->Buffer[ff.rb_word] >> ff.rb_bit) & 0x1;
		result = result | (v << i );
		//printf("i=%d, result=%08x\n", i, result);
	}
	return(~result);
}






void Set_SliceReg(InjectorDescriptor * InjDesc, u32 Y, u32 FAR, u16 val){
	FPGA_ReadFrame(&(InjDesc->HwIcap), SLR_IDX, FAR, InjDesc->FrameData, 0);
	//u32 buffer[FRAME_SIZE];
	FF_CBL ff, ff2;
	memcpy(InjDesc->Buffer, InjDesc->FrameData, FRAME_SIZE*4);

	for(int i=0;i<16;i++){
		ff = get_ff_clb(Y, i, FAR);
		//printf("SR[%d] = %d\n\r", i, (InjDesc->FrameData[ff.sr_word] >> ff.sr_bit) & 0x1);
		if( (val>>i)&0x1 ){
			//set FF=1 (SR bit = 0)
			InjDesc->FrameData[ff.sr_word] &= ~(1 << ff.sr_bit);
		}
		else{
			//reset FF=0 (SR bit = 1)
			InjDesc->FrameData[ff.sr_word] |= (1 << ff.sr_bit);
		}
	}
	FPGA_WriteFrame(&(InjDesc->HwIcap), SLR_IDX, InjDesc->SlrId[SLR_IDX], FAR, InjDesc->FrameData, 0);
	//trigger SR line AFF to DFF
	ff = get_ff_clb(Y, 0, FAR);
	ff2 = get_ff_clb(Y, 8, FAR);
	int c1 = Conditional_BitFlip(InjDesc, 0, ff.enainv_far, ff.enainv_word, ff.enainv_bit);
	int c2 = Conditional_BitFlip(InjDesc, 0, ff2.enainv_far, ff2.enainv_word, ff2.enainv_bit);
	BitFlip(InjDesc, ff.inv_far, ff.inv_word, ff.inv_bit);
	BitFlip(InjDesc, ff.iclk_far, ff.iclk_word, ff.iclk_bit);
	BitFlip(InjDesc, ff.iclk_far, ff.iclk_word, ff.iclk_bit);
	BitFlip(InjDesc, ff.inv_far, ff.inv_word, ff.inv_bit);
	if(c1) BitFlip(InjDesc, ff.enainv_far, ff.enainv_word, ff.enainv_bit);
	if(c2) BitFlip(InjDesc, ff2.enainv_far, ff2.enainv_word, ff2.enainv_bit);

	//trigger SR line EFF to HFF
	ff = get_ff_clb(Y, 4, FAR);
	ff2 = get_ff_clb(Y, 12, FAR);
	c1 = Conditional_BitFlip(InjDesc, 0, ff.enainv_far, ff.enainv_word, ff.enainv_bit);
	c2 = Conditional_BitFlip(InjDesc, 0, ff2.enainv_far, ff2.enainv_word, ff2.enainv_bit);
	BitFlip(InjDesc, ff.inv_far, ff.inv_word, ff.inv_bit);
	BitFlip(InjDesc, ff.iclk_far, ff.iclk_word, ff.iclk_bit);
	BitFlip(InjDesc, ff.iclk_far, ff.iclk_word, ff.iclk_bit);
	BitFlip(InjDesc, ff.inv_far, ff.inv_word, ff.inv_bit);
	if(c1) BitFlip(InjDesc, ff.enainv_far, ff.enainv_word, ff.enainv_bit);
	if(c2) BitFlip(InjDesc, ff2.enainv_far, ff2.enainv_word, ff2.enainv_bit);
	//recover SR bits to original state
	FPGA_WriteFrame(&(InjDesc->HwIcap), SLR_IDX, InjDesc->SlrId[SLR_IDX], FAR, InjDesc->Buffer, 0);
}


int Conditional_BitFlip(InjectorDescriptor * InjDesc, int bitval, u32 FAR, u32 word, u32 bit){
	FPGA_ReadFrame(&(InjDesc->HwIcap), SLR_IDX, FAR, InjDesc->FrameData, 0);
	if( ((InjDesc->FrameData[word] >> bit) & 0x1) == bitval){
		InjDesc->FrameData[word] = InjDesc->FrameData[word] ^ (1<<bit);
		FPGA_WriteFrame(&(InjDesc->HwIcap), SLR_IDX, InjDesc->SlrId[SLR_IDX], FAR, InjDesc->FrameData, 0);
		return(1);
	}
	return(0);
}



void Flip_SliceReg(InjectorDescriptor * InjDesc, u32 Y, u32 label, u32 FAR){
	//printf("Flip_SliceReg: Y=%d, Label=%d, FAR=%08x\n", Y, label, FAR);
	u16 slice_flops = readback_slice_reg(InjDesc, Y, FAR);
		//u16 v1 = slice_flops;
	slice_flops = slice_flops ^ (1 << label);
	Set_SliceReg(InjDesc, Y, FAR, slice_flops);
		//u16 v2 = readback_slice_reg(InjDesc, Y, FAR);
		//printf("Flip FF[%d, %d] = %04x --> %04x (%04x)\n\r", Y, label, v1, v2, slice_flops);
}



u32 get_latch(InjectorDescriptor * InjDesc, u32 len, u32 FAR, u32 * offsets){
	u32 result = 0;
	//u32 buffer[FRAME_SIZE];
	//XFpga_Initialize(&(InjDesc->XFpgaInstance));
	int Status = FPGA_ReadFrame(&(InjDesc->HwIcap), SLR_IDX, FAR, InjDesc->Buffer, 1);
	for(int i =0;i<len;i++){
		u32 word = offsets[i]/32;
		u32 bit = offsets[i]%32;
		u32 v = (InjDesc->Buffer[word] >> bit) & 0x1;
		result = result | (v << i );
	}
	return result;
}



void Flip_BRAM(InjectorDescriptor * InjDesc, u32 FAR, u32 word, u32 mask, u32 Type0_FAR){
	//printf("Pre clock: %08x\n", get_latch(InjDesc, 8, 0x00100c05, offsets));
	FPGA_ReadFrame(&(InjDesc->HwIcap), SLR_IDX, Type0_FAR, InjDesc->Buffer, 1);
	FPGA_ReadFrame(&(InjDesc->HwIcap), SLR_IDX, FAR, InjDesc->FrameData, 1);
		if(InjDesc->DebugMode){
			printf("\tDEBUG-Flip_BRAM: BRAM output after readback = %08x\n\r", XGpio_DiscreteRead(&(InjDesc->Gpio), 1));
		}
	InjDesc->FrameData[word] = InjDesc->FrameData[word] ^ mask;
	mask_bram_frame(InjDesc->FrameData);
	FPGA_WriteFrame(&(InjDesc->HwIcap), SLR_IDX, InjDesc->SlrId[SLR_IDX], FAR, InjDesc->FrameData, 0);
	recover_BRAM_latches(InjDesc, Type0_FAR, InjDesc->Buffer);
		if(InjDesc->DebugMode){
			printf("\tDEBUG-Flip_BRAM: BRAM output after recover_BRAM_latches = %08x\n\r", XGpio_DiscreteRead(&(InjDesc->Gpio), 1));
		}
}


void trigger_bram_clock(InjectorDescriptor * InjDesc, u32 FAR){
	//u32 buffer[FRAME_SIZE];
	//FRAME 0x5
	FPGA_ReadFrame(&(InjDesc->HwIcap), SLR_IDX, (FAR & 0xFFFFFF00) | 0x5, InjDesc->FrameData, 0);
	memcpy(InjDesc->Buffer, InjDesc->FrameData, FRAME_SIZE*4);
	for(int group=0; group<6; group++){
		for(int i=0;i<6;i++){
			u32 word = group*15 + bram_clock_offset_f5[i].word;
			if(group > 2) word += 3;
			InjDesc->FrameData[word] ^= (1 << bram_clock_offset_f5[i].bit);
		}
	}
	FPGA_WriteFrame(&(InjDesc->HwIcap), SLR_IDX, InjDesc->SlrId[SLR_IDX], (FAR & 0xFFFFFF00) | 0x5, InjDesc->FrameData, 0);
	FPGA_WriteFrame(&(InjDesc->HwIcap), SLR_IDX, InjDesc->SlrId[SLR_IDX], (FAR & 0xFFFFFF00) | 0x5, InjDesc->Buffer, 0);
	//FRAME 0x4
	FPGA_ReadFrame(&(InjDesc->HwIcap), SLR_IDX, (FAR & 0xFFFFFF00) | 0x4, InjDesc->FrameData, 0);
	memcpy(InjDesc->Buffer, InjDesc->FrameData, FRAME_SIZE*4);
	for(int group=0; group<6; group++){
		for(int i=0;i<2;i++){
			u32 word = group*15 + bram_clock_offset_f4[i].word;
			if(group > 2) word += 3;
			InjDesc->FrameData[word] ^= (1 << bram_clock_offset_f4[i].bit);
		}
	}
	FPGA_WriteFrame(&(InjDesc->HwIcap), SLR_IDX, InjDesc->SlrId[SLR_IDX], (FAR & 0xFFFFFF00) | 0x4, InjDesc->FrameData, 0);
	FPGA_WriteFrame(&(InjDesc->HwIcap), SLR_IDX, InjDesc->SlrId[SLR_IDX], (FAR & 0xFFFFFF00) | 0x4, InjDesc->Buffer, 0);
}



void trigger_bram_reset(InjectorDescriptor * InjDesc, u32 FAR){
	FPGA_ReadFrame(&(InjDesc->HwIcap), SLR_IDX, (FAR & 0xFFFFFF00) | 0x4, InjDesc->FrameData, 0);
	for(int group=0; group<6; group++){
		for(int i=0;i<6;i++){
			u32 word = group*15 + bram_reset_offset_f4[i].word;
			if(group > 2) word += 3;
			InjDesc->FrameData[word] ^= (1 << bram_reset_offset_f4[i].bit);
		}
	}
	FPGA_WriteFrame(&(InjDesc->HwIcap), SLR_IDX, InjDesc->SlrId[SLR_IDX], (FAR & 0xFFFFFF00) | 0x4, InjDesc->FrameData, 0);
	FPGA_ReadFrame(&(InjDesc->HwIcap), SLR_IDX, (FAR & 0xFFFFFF00) | 0x5, InjDesc->FrameData, 0);
	for(int group=0; group<6; group++){
		for(int i=0;i<2;i++){
			u32 word = group*15 + bram_reset_offset_f5[i].word;
			if(group > 2) word += 3;
			InjDesc->FrameData[word] ^= (1 << bram_reset_offset_f5[i].bit);
		}
	}
	FPGA_WriteFrame(&(InjDesc->HwIcap), SLR_IDX, InjDesc->SlrId[SLR_IDX], (FAR & 0xFFFFFF00) | 0x5, InjDesc->FrameData, 0);
}


void mask_bram_frame(u32 *data){
	for(int group=0; group<6; group++){
	    u32 w1 = group*15 + 3  + (group > 2 ? 3 : 0);
	    u32 w2 = group*15 + 10 + (group > 2 ? 3 : 0);
	    data[w1] &= 0xFFFFEFFF;
	    data[w2] &= 0xEFFFFFFF;
	}
}


void recover_BRAM_latches(InjectorDescriptor * InjDesc, u32 FAR, u32 *Fsnaphot){
	int Status;
	//u32 reference_frame[FRAME_SIZE];
	Status = FPGA_ReadFrame(&(InjDesc->HwIcap), SLR_IDX, FAR, InjDesc->FrameData, 0);
	//memcpy(reference_frame, InjDesc->FrameData, FRAME_SIZE*4);
	for(int group=0; group<6; group++){
		for(int i=0;i<128;i++){
			//get saved latch value
			CMcoord rb = bram_latch_rb[i];
			u32 word = group*15 + rb.word + (group > 2 ? 3 : 0);
			u32 bitval = (Fsnaphot[word] >> rb.bit) & 0x1;
			//set SR latch value
			CMcoord sr = bram_latch_sr[i];
			word = group*15 + sr.word + (group > 2 ? 3 : 0);
			InjDesc->FrameData[word] &= (~(1 << sr.bit));
			InjDesc->FrameData[word] |= (bitval << sr.bit); //(1<<sr.bit);
		}
	}
	FPGA_WriteFrame(&(InjDesc->HwIcap), SLR_IDX, InjDesc->SlrId[SLR_IDX], FAR, InjDesc->FrameData, 0);
	trigger_bram_reset(InjDesc, FAR);
	trigger_bram_clock(InjDesc, FAR);
	trigger_bram_reset(InjDesc, FAR);
	//FPGA_WriteFrame(FAR, reference_frame, InjDesc->SlrId[0], 0);
}


int cmpfunc (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}


u32 input_int(int radix){
	u32 res = 0;
	char8 key;
	while(1) {
		key = inbyte();
		if(radix==10){
			if(key>=48 && key <=57){
				res = res*10+key-48;
			}
		}
		else if(radix==16){
			if(key=='A' || key=='a')      res = res*16+0xA;
			else if(key=='B' || key=='b') res = (res<<4)+0xB;
			else if(key=='C' || key=='c') res = (res<<4)+0xC;
			else if(key=='D' || key=='d') res = (res<<4)+0xD;
			else if(key=='E' || key=='e') res = (res<<4)+0xE;
			else if(key=='F' || key=='f') res = (res<<4)+0xF;
			else if(key>=48 && key <=57)   res = (res<<4)+key-48;
		}
		if(key == '\n' || key =='\r')break;
	}
	return(res);
}


void log_Frame(InjectorDescriptor * InjDesc, u32 SlrID, u32 FAR){
	int Status = FPGA_ReadFrame(&(InjDesc->HwIcap), SlrID, FAR, InjDesc->FrameData, 0);
	if (Status != XST_SUCCESS) {
		if(InjDesc->DebugMode) printf("Failed to Read Frame: SLR: %08x, FAR: %08x, Status: %d\r\n", SlrID, FAR, Status);
		return XST_FAILURE;
	}
	printf("SLR: %08x, FAR: %08x\n\r", InjDesc->SlrId[SlrID], FAR);
	for(int k = 0; k < FRAME_SIZE; k++) printf("Frame Word %03d -> \t %08x \n\r", k  , InjDesc->FrameData[k]);
}




int ProcessFaultDescriptor(InjectorDescriptor * InjDesc, FaultDescriptor *fdesc, int recover){
    //printf("Processing fault descriptor at: 0x%08x\n\r\tId=%d \n\r\tOffset=%d \n\r\tCellType=%d \n\r\tFaultModel=%d \n\r\tCellY=%d \n\r\tCellLabel=%d\n\r", (u32) fdesc, fdesc->Id, fdesc->Offset, fdesc->CellType, fdesc->FaultModel, fdesc->CellY,fdesc->CellLabel);
    switch(fdesc->FaultModel){
        case 0: //SEU (Essential bits and LUTs)
            if( (fdesc->CellType == 0) || (fdesc->CellType == 1) ){
                //if(InjDesc->DebugMode) printf("Injecting SEU EssentialBit/LUT: %d\n\r", fdesc->Id);
                FlipBitmask(InjDesc, fdesc->FAR, fdesc->word, fdesc->mask);
            }
            else{
                printf("ProcessFaultDescriptor: fault model SEU not defined for CellType=%d\n\r", fdesc->CellType);
            }
            break;

        case 1: //Bit-Flip
            if( fdesc->CellType == 2 ){
                if(recover == 0){
                    //if(InjDesc->DebugMode) printf("Injecting Bit-Flip in Flip-Flop: %d\n\r", fdesc->Id);
                    Flip_SliceReg(InjDesc, fdesc->CellY, fdesc->CellLabel, fdesc->FAR);
                }
            }
            else if(fdesc->CellType == 3){
                //if(InjDesc->DebugMode) printf("Injecting Bit-Flip in BRAM: %d\n\r", fdesc->Id);
                Flip_BRAM(InjDesc, fdesc->FAR, fdesc->word, fdesc->mask, fdesc->Type0_FAR);
            }
            else{
                printf("ProcessFaultDescriptor: fault model Bit-Flip not defined for CellType=%d\n\r", fdesc->CellType);
            }
            break;

        default:
            printf("ProcessFaultDescriptor: undefined fault model %d\n\r", fdesc->FaultModel);
            break;
    };
	return(0);
}


int EmulateSingleSEU(InjectorDescriptor * InjDesc, int FdescOffset){
	FaultDescriptor fdesc;
	fdesc =	*(InjDesc->fault_list_ptr + FdescOffset);
	if(fdesc.time > 0) usleep_MB(fdesc.time);
	if(fdesc.duration > 0){
		int Status = FPGA_ReadFrame(&(InjDesc->HwIcap), fdesc.SLR, fdesc.FAR, InjDesc->FrameData, 0);
		InjDesc->FrameData[fdesc.word] = InjDesc->FrameData[fdesc.word] ^ fdesc.mask;
		Status = FPGA_WriteFrame(&(InjDesc->HwIcap), fdesc.SLR, InjDesc->SlrId[fdesc.SLR], fdesc.FAR, InjDesc->FrameData, 0);
		usleep_MB(fdesc.duration);
		InjDesc->FrameData[fdesc.word] = InjDesc->FrameData[fdesc.word] ^ fdesc.mask;
		Status = FPGA_WriteFrame(&(InjDesc->HwIcap), fdesc.SLR, InjDesc->SlrId[fdesc.SLR], fdesc.FAR, InjDesc->FrameData, 0);
	}
	else {
		emulatePulse(InjDesc, fdesc.SLR, InjDesc->SlrId[fdesc.SLR], fdesc.FAR, fdesc.word, fdesc.mask);
	}
	return(0);
}


void BitFlip(InjectorDescriptor * InjDesc, u32 FAR, u32 word, u32 bit){
	FPGA_ReadFrame(&(InjDesc->HwIcap), SLR_IDX, FAR, InjDesc->FrameData, 0);
	InjDesc->FrameData[word] = InjDesc->FrameData[word] ^ (1<<bit);
	FPGA_WriteFrame(&(InjDesc->HwIcap), SLR_IDX, InjDesc->SlrId[SLR_IDX], FAR, InjDesc->FrameData, 0);
}


int FlipBitmask(InjectorDescriptor * InjDesc, u32 FAR, u32 word, u32 mask){
	int Status = FPGA_ReadFrame(&(InjDesc->HwIcap), SLR_IDX, FAR, InjDesc->FrameData, 0);
	if (Status != XST_SUCCESS) {
		if(InjDesc->DebugMode) printf("Failed to Read Frame: %d \r\n", Status);
		return XST_FAILURE;
	}
	InjDesc->FrameData[word] = InjDesc->FrameData[word] ^ mask;
	Status = FPGA_WriteFrame(&(InjDesc->HwIcap), SLR_IDX, InjDesc->SlrId[SLR_IDX], FAR, InjDesc->FrameData, 0);
	if (Status != XST_SUCCESS) {
		if(InjDesc->DebugMode) printf("Failed to Write Frame: %d \r\n", Status);
		return XST_FAILURE;
	}
	return(0);
}




int FPGA_ReadFrame(XHwIcap *InstancePtr, u32 SlrConfigIndex, u32 FAR, u32 *FrameBuffer, int Capture) {
	u32 Packet;
	u32 Data;
	int Status;
	u32 WriteBuffer[200];
	u32 Index = 0;

	Xil_AssertNonvoid(InstancePtr != NULL);
	Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);
	Xil_AssertNonvoid(FrameBuffer != NULL);

	for(int i=0;i<SlrConfigIndex;i++){
		WriteBuffer[Index++] = 0x3003c000;
		WriteBuffer[Index++] = 0x50cbd9dc; //0x57FFFFFF;
		WriteBuffer[Index++] = 0xFFFFFFFF;
		WriteBuffer[Index++] = 0x000000BB;
		WriteBuffer[Index++] = 0x11220044;
		WriteBuffer[Index++] = 0xFFFFFFFF;
		WriteBuffer[Index++] = 0xAA995566;
		WriteBuffer[Index++] = 0x20000000;
	}

	// DUMMY and SYNC
	WriteBuffer[Index++] = XHI_DUMMY_PACKET;
	WriteBuffer[Index++] = XHI_BUS_WTH_PACKET;
	WriteBuffer[Index++] = XHI_BUS_DET_PACKET;
	WriteBuffer[Index++] = XHI_DUMMY_PACKET;
	WriteBuffer[Index++] = XHI_SYNC_PACKET;
	WriteBuffer[Index++] = XHI_NOOP_PACKET;

	// Reset CRC
	/*
	WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_CMD) | 1;
	WriteBuffer[Index++] = XHI_CMD_RCRC;
	WriteBuffer[Index++] = XHI_NOOP_PACKET;
	WriteBuffer[Index++] = XHI_NOOP_PACKET;
	*/
	// GCAPTURE
	if(Capture>0){
		WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_CMD) | 1; //Type1_Packet(2, XHI_CMD, 0x1);
		WriteBuffer[Index++] = 0x30008001;	//CMD write
		WriteBuffer[Index++] = 0x00000000;	//NULL
		WriteBuffer[Index++] = 0x3000C001;	//MSK write
		WriteBuffer[Index++] = 0x00800000;	//CAPTURE bit[23]
		WriteBuffer[Index++] = 0x30030001;	//CTL1 write
		WriteBuffer[Index++] = 0x00800000;	//CAPTURE bit[23]
	}

	for(int i = 0; i < 8; i++) {
		WriteBuffer[Index++] = XHI_NOOP_PACKET;
	}

	// Setup CMD register to read configuration
	WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_CMD) | 1;
	WriteBuffer[Index++] = XHI_CMD_RCFG;
	WriteBuffer[Index++] = XHI_NOOP_PACKET;

	// Setup FAR register
	Packet = XHwIcap_Type1Write(XHI_FAR) | 1;
	Data = FAR; //XHwIcap_Custom_SetupFar7series(Top, Block, HClkRow,  MajorFrame, MinorFrame);
	WriteBuffer[Index++] = Packet;
	WriteBuffer[Index++] = Data;

	// Create Type one packet
	Packet = XHwIcap_Type1Read(XHI_FDRO);
	WriteBuffer[Index++] = Packet;
	WriteBuffer[Index++] = XHI_TYPE_2_READ | ((InstancePtr->WordsPerFrame << 1)+READBACK_PAD_WORDS );

	for(int i = 0; i < 32; i++) {
		WriteBuffer[Index++] = XHI_NOOP_PACKET;
	}

	//Write the data to the FIFO and initiate the transfer of data present in the FIFO to the ICAP device
	Status = XHwIcap_DeviceWrite(InstancePtr, (u32 *)&WriteBuffer[0], Index);
		if (Status != XST_SUCCESS)  { return XST_FAILURE; }

	// Wait till the write is done
	while (XHwIcap_IsDeviceBusy(InstancePtr) != FALSE);

	//read pad frame and discard
	Status = XHwIcap_DeviceRead(InstancePtr, FrameBuffer, InstancePtr->WordsPerFrame+READBACK_PAD_WORDS-2);
	//read data frame
	Status = XHwIcap_DeviceRead(InstancePtr, FrameBuffer, InstancePtr->WordsPerFrame);
		if (Status != XST_SUCCESS)  { return XST_FAILURE; }

	/* Send DESYNC command*/
		/*
	Status = XHwIcap_CommandDesync(InstancePtr);
		if (Status != XST_SUCCESS)  { return XST_FAILURE; }
	*/
		Index = 0;
		if(Capture>0){
			WriteBuffer[Index++] = 0x3000C001; //MSK write
			WriteBuffer[Index++] = 0x00800000; //CAPTURE bit[23]
			WriteBuffer[Index++] = 0x30030001; //CTL1 write
			WriteBuffer[Index++] = 0x00000000; //CAPTURE bit[23]
			WriteBuffer[Index++] = 0x20000000;
			WriteBuffer[Index++] = 0x20000000;
		}
		WriteBuffer[Index++] = 0x30008001U; /* Type 1 Write 1 Word to CMD */
		WriteBuffer[Index++] = 0x00000007U; /* RCRC Command */
		WriteBuffer[Index++] = 0x20000000U; /* Type 1 NOOP Word 0 */
		WriteBuffer[Index++] = 0x30008001U; /* Type 1 Write 1 Word to CMD */
		WriteBuffer[Index++] = 0x0000000DU; /* DESYNC Command */
		WriteBuffer[Index++] = XHI_DUMMY_PACKET;
		WriteBuffer[Index++] = XHI_DUMMY_PACKET;
		Status = XHwIcap_DeviceWrite(InstancePtr, (u32 *)&WriteBuffer[0], Index);
			if (Status != XST_SUCCESS)  { return XST_FAILURE; }

	return XST_SUCCESS;
}







int FPGA_WriteFrame(XHwIcap *InstancePtr, u32 SlrConfigIndex, u32 SlrIDcode, u32 FAR, u32 *FrameBuffer, int Restore)
{
	u32 TotalWords;
	int Status;
	u32 WriteBuffer[200];
	u32 PadFrame[FRAME_SIZE+READBACK_PAD_WORDS];
	u32 Index = 0;


	Xil_AssertNonvoid(InstancePtr != NULL);
	Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);
	Xil_AssertNonvoid(FrameBuffer != NULL);


	// DUMMY and SYNC
	WriteBuffer[Index++] = XHI_DUMMY_PACKET;
	WriteBuffer[Index++] = XHI_BUS_WTH_PACKET;
	WriteBuffer[Index++] = XHI_BUS_DET_PACKET;
	WriteBuffer[Index++] = XHI_NOOP_PACKET;
	WriteBuffer[Index++] = XHI_SYNC_PACKET;
	WriteBuffer[Index++] = XHI_NOOP_PACKET;

	// Reset CRC
	WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_CMD) | 1;
	WriteBuffer[Index++] = XHI_CMD_RCRC;
	WriteBuffer[Index++] = XHI_NOOP_PACKET;
	//WriteBuffer[Index++] = XHI_NOOP_PACKET;


	// ID register
	WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_IDCODE) | 1;
	WriteBuffer[Index++] = SlrIDcode;

	// Setup CMD register - write configuration
	WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_CMD) | 1;
	WriteBuffer[Index++] = XHI_CMD_WCFG;
	WriteBuffer[Index++] = XHI_NOOP_PACKET;

	// Setup FAR
	WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_FAR) | 1;
	WriteBuffer[Index++] = FAR;
	//printf("WRITE-FAR: %8x\n\r", WriteBuffer[Index-1]);
	WriteBuffer[Index++] = XHI_NOOP_PACKET;

	// Setup Packet header.
	TotalWords = (InstancePtr->WordsPerFrame << 1) + READBACK_PAD_WORDS;
	if (TotalWords < XHI_TYPE_1_PACKET_MAX_WORDS)  {
		WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_FDRI) | TotalWords;
	}
	else {
		WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_FDRI);
		WriteBuffer[Index++] = XHI_TYPE_2_WRITE | TotalWords;
	}

	//debug trace
	//for(u32 v = 0;v<Index;v++){ printf("Buf[%u] = %08x\n", v, WriteBuffer[v]); }

	// Write the Header data into the FIFO and intiate the transfer of data present in the FIFO to the ICAP device
	Status = XHwIcap_DeviceWrite(InstancePtr, (u32 *)&WriteBuffer[0], Index);
	if (Status != XST_SUCCESS)  { return XST_FAILURE; }

	// Write the modified frame data.
	Status = XHwIcap_DeviceWrite(InstancePtr, FrameBuffer, InstancePtr->WordsPerFrame);
		if (Status != XST_SUCCESS) { return XST_FAILURE;}

	// Write out the pad frame. The pad frame was read from the device before the data frame.
	for(int i=0;i<FRAME_SIZE+READBACK_PAD_WORDS;i++){ PadFrame[i]=0xFFFFFFFF; };
	Status = XHwIcap_DeviceWrite(InstancePtr, &PadFrame[0], InstancePtr->WordsPerFrame+READBACK_PAD_WORDS);
	if (Status != XST_SUCCESS) { return XST_FAILURE; }

	Index = 0;
	//Restore Registers
	if(Restore){
		WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_CMD) | 1; //Type1_Packet(2, XHI_CMD, 0x1);
		WriteBuffer[Index++] = XHI_CMD_GRESTORE;
		WriteBuffer[Index++] = XHI_NOOP_PACKET;
	}

	// Park the FAR
	WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_FAR) | 1;
	WriteBuffer[Index++] = XHwIcap_SetupFar(0, 0, 3, 33, 0);
	// Append CRC
	WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_CMD) | 1;
	WriteBuffer[Index++] = XHI_CMD_RCRC;
	WriteBuffer[Index++] = XHI_NOOP_PACKET;
	WriteBuffer[Index++] = XHI_NOOP_PACKET;
	// Initiate startup sequence
	WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_CMD) | 1;
	WriteBuffer[Index++] = XHI_CMD_START;
	WriteBuffer[Index++] = XHI_NOOP_PACKET;
	// Desynchronize the device
	WriteBuffer[Index++] = (XHwIcap_Type1Write(XHI_CMD) | 1);
	WriteBuffer[Index++] = XHI_CMD_DESYNCH;
	for(int i = 0; i < 10; i++) {
		WriteBuffer[Index++] = XHI_NOOP_PACKET;
	}

	//debug trace
	//for(u32 v = 0;v<Index;v++){ printf("Buf[%u] = %08x\n", v, WriteBuffer[v]); }

	// Intiate the transfer of data present in the FIFO to the ICAP device
	Status = XHwIcap_DeviceWrite(InstancePtr, &WriteBuffer[0], Index);
		if (Status != XST_SUCCESS)  { return XST_FAILURE; }


	return XST_SUCCESS;
}



int emulatePulse(InjectorDescriptor * InjDesc, u32 SlrConfigIndex, u32 SlrIDcode, u32 FAR, u32 Word, u32 Mask)
{
	u32 TotalWords;
	int Status;
	u32 WriteBuffer[800];
	u32 Index = 0;
	XHwIcap *InstancePtr = &(InjDesc->HwIcap);

	Xil_AssertNonvoid(InstancePtr != NULL);
	Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);
	Status = FPGA_ReadFrame(InstancePtr, SlrConfigIndex, FAR, InjDesc->FrameData, 0);

	// DUMMY and SYNC
	WriteBuffer[Index++] = XHI_DUMMY_PACKET;
	WriteBuffer[Index++] = XHI_BUS_WTH_PACKET;
	WriteBuffer[Index++] = XHI_BUS_DET_PACKET;
	WriteBuffer[Index++] = XHI_NOOP_PACKET;
	WriteBuffer[Index++] = XHI_SYNC_PACKET;
	WriteBuffer[Index++] = XHI_NOOP_PACKET;
	// Reset CRC
	WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_CMD) | 1;
	WriteBuffer[Index++] = XHI_CMD_RCRC;
	WriteBuffer[Index++] = XHI_NOOP_PACKET;
	//WriteBuffer[Index++] = XHI_NOOP_PACKET;
	// ID register
	WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_IDCODE) | 1;
	WriteBuffer[Index++] = SlrIDcode;
	for(int k=0;k<2;k++){
		// Setup CMD register - write configuration
		WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_CMD) | 1;
		WriteBuffer[Index++] = XHI_CMD_WCFG;
		WriteBuffer[Index++] = XHI_NOOP_PACKET;
		// Setup FAR
		WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_FAR) | 1;
		WriteBuffer[Index++] = FAR;
		//printf("WRITE-FAR: %8x\n\r", WriteBuffer[Index-1]);
		WriteBuffer[Index++] = XHI_NOOP_PACKET;
		// Setup Packet header.
		TotalWords = (InstancePtr->WordsPerFrame * 2) + READBACK_PAD_WORDS;
		WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_FDRI) | TotalWords;
		// Write frame data.
		InjDesc->FrameData[Word] = InjDesc->FrameData[Word] ^ Mask;
		for(int i=0;i<InstancePtr->WordsPerFrame;i++) WriteBuffer[Index++] = InjDesc->FrameData[i];
		for(int i=0;i<InstancePtr->WordsPerFrame+READBACK_PAD_WORDS;i++) WriteBuffer[Index++] = 0xFFFFFFFF;
	}
	// Park the FAR
	WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_FAR) | 1;
	WriteBuffer[Index++] = XHwIcap_SetupFar(0, 0, 3, 33, 0);
	// Append CRC
	WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_CMD) | 1;
	WriteBuffer[Index++] = XHI_CMD_RCRC;
	WriteBuffer[Index++] = XHI_NOOP_PACKET;
	WriteBuffer[Index++] = XHI_NOOP_PACKET;
	// Initiate startup sequence
	WriteBuffer[Index++] = XHwIcap_Type1Write(XHI_CMD) | 1;
	WriteBuffer[Index++] = XHI_CMD_START;
	WriteBuffer[Index++] = XHI_NOOP_PACKET;
	// Desynchronize the device
	WriteBuffer[Index++] = (XHwIcap_Type1Write(XHI_CMD) | 1);
	WriteBuffer[Index++] = XHI_CMD_DESYNCH;
	for(int i = 0; i < 10; i++) {
		WriteBuffer[Index++] = XHI_NOOP_PACKET;
	}

	Status = XHwIcap_DeviceWrite(InstancePtr, (u32 *)&WriteBuffer[0], Index);
	if (Status != XST_SUCCESS)  { return XST_FAILURE; }

	//FPGA_ReadFrame(InstancePtr, SlrConfigIndex, FAR, InjDesc->FrameData, 0);
	//for(int k = 0; k < FRAME_SIZE; k++) printf("After Frame (%03d) = %08x \n\r", k  , InjDesc->FrameData[k]);

	return XST_SUCCESS;
}
